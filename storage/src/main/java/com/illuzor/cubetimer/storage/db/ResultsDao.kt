package com.illuzor.cubetimer.storage.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.illuzor.cubetimer.storage.model.ResultEntity
import kotlinx.coroutines.flow.Flow

@Dao
internal interface ResultsDao {

    @Insert
    suspend fun insert(result: ResultEntity)

    @Query(value = "SELECT * FROM results WHERE puzzle = :puzzle ORDER BY date DESC")
    fun getAllResults(puzzle: String): Flow<List<ResultEntity>>

    @Query(value = "SELECT * FROM results WHERE puzzle = :puzzle ORDER BY date DESC LIMIT :amount")
    suspend fun getLastResults(puzzle: String, amount: Int): List<ResultEntity>

    @Query(
        value =
        """SELECT AVG(time) FROM
            (SELECT * FROM results WHERE puzzle = :puzzle ORDER BY date DESC LIMIT :amount)"""
    )

    suspend fun getLastAvgTime(puzzle: String, amount: Int): Long

    @Query(value = "SELECT MIN(time) FROM results WHERE puzzle = :puzzle")
    suspend fun getBestTime(puzzle: String): Long

    @Query(value = "SELECT MAX(time) FROM results WHERE puzzle = :puzzle")
    suspend fun getWorstTime(puzzle: String): Long

    @Query(value = "SELECT COUNT(*) FROM results  WHERE puzzle = :puzzle")
    suspend fun resultsCount(puzzle: String): Int

    @Delete
    suspend fun delete(result: ResultEntity)
}
