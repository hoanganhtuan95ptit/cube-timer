package com.illuzor.cubetimer.utils

import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class InstanceHolderTest {

    private val testHolder = InstanceHolder<String>()

    @After
    fun cleanup() = testHolder.cleanup()

    @Test(expected = NullPointerException::class)
    fun `throws on value access if empty`() {
        testHolder.value
    }

    @Test
    fun `access value if not empty`() {
        val string = "hello"

        testHolder.setValue(string)

        assertEquals(string, testHolder.value)
    }

    @Test
    fun `hasValue false if empty`() {
        assertFalse(testHolder.hasValue)
    }

    @Test
    fun `hasValue true if not empty`() {
        testHolder.setValue("42")

        assertTrue(testHolder.hasValue)
    }

    @Test
    fun `hasValue false after cleanup`() {
        testHolder.setValue("42")
        testHolder.cleanup()

        assertFalse(testHolder.hasValue)
    }
}
