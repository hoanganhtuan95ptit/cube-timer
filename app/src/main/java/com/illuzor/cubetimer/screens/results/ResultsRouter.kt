package com.illuzor.cubetimer.screens.results

import com.illuzor.cubetimer.screens.RootRouter

class ResultsRouter(private val rootRouter: RootRouter) {

    fun goBack() = rootRouter.goBack()
}
