package com.illuzor.cubetimer.storage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.illuzor.cubetimer.storage.model.ResultEntity

@Database(entities = [ResultEntity::class], version = 1, exportSchema = true)
internal abstract class TimerDatabase : RoomDatabase() {
    abstract fun getResultsDao(): ResultsDao
}
