package com.illuzor.cubetimer.storage

import android.content.Context
import androidx.test.core.app.ApplicationProvider

internal val context: Context = ApplicationProvider.getApplicationContext()
