package com.illuzor.cubetimer.utils

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class TimerTest {

    @Test
    fun half_second_accuracy() = testAccuracy(500)

    @Test
    fun one_second_accuracy() = testAccuracy(1000)

    @Test
    fun two_seconds_accuracy() = testAccuracy(2000)

    @Test
    fun three_seconds_accuracy() = testAccuracy(3000)

    @Test
    fun ten_seconds_accuracy() = testAccuracy(10_000)

    private fun testAccuracy(time: Long) {
        val timer = Timer(10)
        var counter = 0L
        timer.onTick {
            counter = it
        }

        timer.start()
        Thread.sleep(time)
        timer.stop()

        assertTrue(counter in time - 100..time + 100)
    }

    @Test(expected = IllegalStateException::class)
    fun start_without_listener_exception() = Timer().start()

    @Test(expected = IllegalStateException::class)
    fun start_twice_exception() {
        Timer().apply {
            onTick { Unit }
            start()
            start()
        }
    }

    @Test(expected = IllegalStateException::class)
    fun stop_before_start_exception() = Timer().stop()

    @Test(expected = IllegalStateException::class)
    fun start_double_stop_exception() {
        Timer().apply {
            onTick { }
            start()
            stop()
            stop()
        }
    }

    @Test
    fun all_ticks_are_different() {
        val timer = Timer()
        val ticks = mutableListOf<Long>()
        timer.onTick {
            ticks.add(it)
        }
        timer.start()
        Thread.sleep(1100)
        timer.stop()
        assertEquals(ticks, ticks.toSet().toList())
    }

    @Test
    fun each_tick_is_bigger_than_prev() {
        val timer = Timer()
        val ticks = mutableListOf<Long>()
        timer.onTick {
            ticks.add(it)
        }
        timer.start()
        Thread.sleep(1100)
        timer.stop()
        assertEquals(ticks, ticks.sorted())
    }

    @Test
    fun var_zeroing_after_restart() {
        var value = 0L
        val timer = Timer()
        timer.onTick {
            value = it
        }

        timer.start()
        Thread.sleep(300)
        timer.stop()

        val firstValue = value

        timer.start()
        Thread.sleep(100)
        timer.stop()

        assertTrue(firstValue > value)
    }

    @Test
    fun milliseconds_is_equals_to_latest_tick() {
        var value = 0L
        val timer = Timer()
        timer.onTick {
            value = it
        }

        timer.start()
        Thread.sleep(300)
        timer.stop()

        assertEquals(value, timer.milliseconds)
    }
}
