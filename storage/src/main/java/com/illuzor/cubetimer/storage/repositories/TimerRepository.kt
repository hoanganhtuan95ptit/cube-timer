package com.illuzor.cubetimer.storage.repositories

import com.illuzor.cubetimer.storage.model.ResultEntity

interface TimerRepository {
    suspend fun insert(result: ResultEntity)
    suspend fun getLastAvgTime(puzzle: String, amount: Int): Long
    suspend fun getLatestTime(puzzle: String): Long
    suspend fun getBestTime(puzzle: String): Long
    suspend fun getWorstTime(puzzle: String): Long
    suspend fun resultsCount(puzzle: String): Int
}
