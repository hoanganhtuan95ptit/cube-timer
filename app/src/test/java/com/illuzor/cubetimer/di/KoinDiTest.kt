package com.illuzor.cubetimer.di

import android.os.Build
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.koin.test.check.checkModules
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])
class KoinDiTest : KoinTest {

    @Test
    fun `check koin modules`() = getKoin().checkModules()
}
