package com.illuzor.cubetimer.storage.repositories

import com.illuzor.cubetimer.storage.model.ResultEntity
import kotlinx.coroutines.flow.Flow

interface ResultsRepository {
    fun getResults(puzzle: String): Flow<List<ResultEntity>>
}
