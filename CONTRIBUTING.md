# CONTRIBUTING

### Issues
If you have a question, proposal, feature request or found errors, create an [issue](https://gitlab.com/illuzor/cube-timer/issues).

### Participation in the project
If you want to help with code:
* Fork the repository;
* Clone it to your computer;
* Create a new branch;
* Edit code;
* Commit and push changes;
* Create new merge request.
