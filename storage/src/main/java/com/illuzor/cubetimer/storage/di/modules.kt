package com.illuzor.cubetimer.storage.di

import androidx.room.Room
import com.illuzor.cubetimer.storage.db.TimerDatabase
import com.illuzor.cubetimer.storage.repositories.ResultsRepository
import com.illuzor.cubetimer.storage.repositories.ResultsRepositoryImpl
import com.illuzor.cubetimer.storage.repositories.TimerRepository
import com.illuzor.cubetimer.storage.repositories.TimerRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val DB_NAME = "timer.db"

val storageModule = module {
    single {
        Room.databaseBuilder(androidContext(), TimerDatabase::class.java, DB_NAME)
            .build()
            .getResultsDao()
    }

    factory<TimerRepository> { TimerRepositoryImpl(get()) }
    factory<ResultsRepository> { ResultsRepositoryImpl(get()) }
}
