package com.illuzor.cubetimer.model

data class RecentResults(
    val latest: String,
    val best: String,
    val worst: String,
    val avgLast5: String,
    val avgLast10: String
) {
    companion object {
        private const val EMPTY_RESULT = "00:00.00"

        fun empty() =
            RecentResults(EMPTY_RESULT, EMPTY_RESULT, EMPTY_RESULT, EMPTY_RESULT, EMPTY_RESULT)
    }
}
