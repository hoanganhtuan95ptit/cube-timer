package com.illuzor.cubetimer

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.illuzor.cubetimer.utils.InstanceHolder
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val navControllerHolder: InstanceHolder<NavController> by inject()

    override fun onResume() {
        super.onResume()
        navControllerHolder.setValue(findNavController(R.id.container))
    }

    override fun onPause() {
        super.onPause()
        navControllerHolder.cleanup()
    }
}
